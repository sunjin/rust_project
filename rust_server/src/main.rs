#![deny(warnings)]

extern crate rust_server;
use rust_server::common::config;
use rust_server::error::Result;
use rust_server::infrastructure;

#[tokio::main]
async fn main() -> Result<()> {
    // init config
    // 一度呼ばないと初期化されないため
    println!("config is {:?}", *config::CONFIG_DATA);

    // init
    let mongo_init = infrastructure::mongodb::init().await;
    if let Err(e) = mongo_init {
        panic!("mongoのセットアップでエラーが発生しました : {:?}", e);
    }

    println!("rust server");
    infrastructure::http::run().await
}
