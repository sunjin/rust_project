use crate::error::Result;
use hyper::{Body, Request, Response};

// get_error errorを発生させるテスト
pub fn get_error(_: Request<Body>) -> Result<Response<Body>> {
    Ok(Response::new("Error Test".into()))
}
