use crate::domain::book;
use crate::error::Result;
use crate::repository::mongodb::user::user_book_repository::*;
use hyper::{Body, Request, Response, StatusCode};
use std::collections::HashMap;
use url::form_urlencoded;

pub async fn add_book(req: Request<Body>) -> Result<Response<Body>> {
    // get param
    let b = hyper::body::to_bytes(req).await?;

    let params = form_urlencoded::parse(b.as_ref())
        .into_owned()
        .collect::<HashMap<String, String>>();

    let title = if let Some(t) = params.get("title") {
        t
    } else {
        return response_failed("title");
    };
    let desc = if let Some(d) = params.get("desc") {
        d
    } else {
        return response_failed("desc");
    };

    let page_cnt = if let Some(p) = params.get("page_cnt") {
        if let Ok(v) = p.parse::<i32>() {
            v
        } else {
            return response_failed("page_cnt parse");
        }
    } else {
        return response_failed("page_cnt");
    };

    // insert
    let _ = insert(UserBook {
        title: title.to_string(),
        desc: desc.to_string(),
        page_cnt: page_cnt,
    })
    .await;

    Ok(Response::new("success".into()))
}

// get_book 本情報を取得する
pub async fn get_book(req: Request<Body>) -> Result<Response<Body>> {
    println!("request is {:?}", req);
    let hash_query: HashMap<_, _> = url::form_urlencoded::parse(
        req.uri()
            .query()
            .unwrap_or_else(|| panic!("not found query"))
            .as_bytes(),
    )
    .into_owned()
    .collect();

    println!("hash query is {:?}", hash_query);

    let title = hash_query.get("title");
    if let Some(title) = title {
        let result = book::get_book(title).await;

        return match result {
            Some(user_book) => {
                let json_str = serde_json::to_string(&user_book).unwrap();
                Ok(Response::new(json_str.into()))
            }
            None => Ok(Response::new("book not found...".into())),
        };
    }

    Ok(Response::new("not found query -> title".into()))
}

fn response_failed(param: &str) -> Result<Response<Body>> {
    let msg = format!("param error {}", param);

    Ok(Response::builder()
        .status(StatusCode::UNPROCESSABLE_ENTITY)
        .body(msg.into())
        .unwrap())
}
