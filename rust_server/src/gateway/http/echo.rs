use futures_util::TryStreamExt;

use hyper::{Body, Request, Response, StatusCode};

use std::collections::HashMap;
use url::form_urlencoded;

use crate::error;
use crate::error::Result;

static MISSING: &[u8] = b"Missing field";
static NOTNUMERIC: &[u8] = b"Number field is not numeric";
static INDEX: &[u8] = b"<html><body><form action=\"parameter\" method=\"post\">Name: <input type=\"text\" name=\"name\"><br>Number: <input type=\"text\" name=\"number\"><br><input type=\"submit\"></body></html>";

// echo
pub fn echo(req: Request<Body>) -> Result<Response<Body>> {
    Ok(Response::new(req.into_body()))
}

// echo_uppercase
pub fn echo_uppercase(req: Request<Body>) -> Result<Response<Body>> {
    let chunk_stream = req.into_body().map_ok(|chunk| {
        chunk
            .iter()
            .map(|byte| byte.to_ascii_uppercase())
            .collect::<Vec<u8>>()
    });

    Ok(Response::new(Body::wrap_stream(chunk_stream)))
}

pub fn echo_form(_: Request<Body>) -> Result<Response<Body>> {
    Ok(Response::new(INDEX.into()))
}

pub async fn echo_parameter(req: Request<Body>) -> Result<Response<Body>> {
    let b = hyper::body::to_bytes(req).await?;
    let params = form_urlencoded::parse(b.as_ref())
        .into_owned()
        .collect::<HashMap<String, String>>();

    let name = params.get("name");
    if name.is_none() {
        return Err(error::Error::new(
            error::Kind::Logic(error::Logic::ParamNotFound),
            Some("nameというパラメータがありませんでした".to_string()),
        ));
    }

    let number = if let Some(n) = params.get("number") {
        if let Ok(v) = n.parse::<f64>() {
            v
        } else {
            return Ok(Response::builder()
                .status(StatusCode::UNPROCESSABLE_ENTITY)
                .body(NOTNUMERIC.into())
                .unwrap());
        }
    } else {
        return Ok(Response::builder()
            .status(StatusCode::UNPROCESSABLE_ENTITY)
            .body(MISSING.into())
            .unwrap());
    };

    let body = format!("Hello {}, your number is {}", name.unwrap(), number);
    Ok(Response::new(body.into()))
}
