use crate::error::Result;
use hyper::{Body, Request, Response};

// get
pub fn get(_: Request<Body>) -> Result<Response<Body>> {
    let message = String::from("こんにちは");
    Ok(Response::new(Body::from(message)))
}

// get_html htmlの文字列を返すようにしてみる
pub fn get_html(_: Request<Body>) -> Result<Response<Body>> {
    let body = String::from(
        "
    <html>
        <head>
            <title>hello rust http</title>
            <meta charset=\"UTF-8\">
        </head>
        <body>
            <h1>HelloWorld</h1>
            <h2>こんにちは！</h2>
        </body>
    </html>",
    );

    Ok(Response::new(Body::from(body)))
}
