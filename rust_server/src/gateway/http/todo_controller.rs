use crate::domain::todo_service;
use crate::error::{Error, Logic, Result};
use hyper::{Body, Request, Response};
use std::collections::HashMap;

// get todoリストをすべて取得する
pub async fn get(_: Request<Body>) -> Result<Response<Body>> {
    let todo_list = todo_service::get().await?;
    let json_str = serde_json::to_string(&todo_list)?;

    Ok(Response::new(json_str.into()))
}

// create todoリストの作成
pub async fn create(req: Request<Body>) -> Result<Response<Body>> {
    // get param
    let query = req
        .uri()
        .query()
        .ok_or(Error::new_logic(
            Logic::NotFoundQuery,
            "queryのparseに失敗しました",
        ))?
        .as_bytes();
    let hash_query: HashMap<_, _> = url::form_urlencoded::parse(query).into_owned().collect();

    let title = hash_query.get("title").ok_or(Error::new_logic(
        Logic::ParamNotFound,
        "param: titleが見つかりませんでした",
    ))?;
    let desc = hash_query.get("desc").ok_or(Error::new_logic(
        Logic::ParamNotFound,
        "param: descが見つかりませんでした",
    ))?;

    // add todo
    todo_service::add_todo(title, desc).await?;

    let msg = format!(
        "make todo -> title: {}, desc: {}",
        title.to_string(),
        desc.to_string()
    );

    Ok(Response::new(msg.into()))
}

// delete todoの削除
