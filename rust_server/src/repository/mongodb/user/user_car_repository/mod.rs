use crate::error::Result;
use crate::infrastructure::mongodb::{collection::Collection, connect::get_user_collection};
use mongodb::bson::doc;
use serde::{Deserialize, Serialize};

const THIS_COLLECTION_NAME: &'static str = "USER_COLLECTION_NAME";

#[derive(Serialize, Deserialize)]
pub struct UserCar {
    pub name: String,
    pub size: i32,
}

// get_db
fn get_db() -> Collection {
    get_user_collection(THIS_COLLECTION_NAME)
}

// insert 追加
pub async fn insert(user_car: UserCar) -> Result<()> {
    get_db().insert_one(user_car, None).await?;
    Ok(())
}
