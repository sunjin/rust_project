use crate::error::Result;
use crate::infrastructure::mongodb::{collection::Collection, connect::get_user_collection};
use mongodb::bson::doc;
use mongodb::bson::oid::ObjectId;
use serde::{Deserialize, Serialize};

const THIS_COLLECTION_NAME: &'static str = "USER_TODO";

#[derive(Serialize, Deserialize)]
// #[model(index(keys=r#"doc!{"title": 1}"#, options=r#"doc!{"unique": true}"#))]
pub struct UserTodo {
    #[serde(rename = "_id", skip_serializing_if = "Option::is_none")]
    pub id: Option<ObjectId>,

    pub title: String,
    pub desc: String,
}

// get_db
fn get_db() -> Collection {
    get_user_collection(THIS_COLLECTION_NAME)
}

// insert
pub async fn insert(title: &str, desc: &str) -> Result<()> {
    let user_todo = UserTodo {
        id: None,
        title: title.to_string(),
        desc: desc.to_string(),
    };
    get_db().insert_one(user_todo, None).await?;
    Ok(())
}

// find_all
pub async fn find_all() -> Result<Vec<UserTodo>> {
    get_db().find::<UserTodo>(doc![], None).await
}
