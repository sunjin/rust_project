use crate::infrastructure::mongodb::{collection::Collection, connect::get_user_collection};
use mongodb::bson::doc;
use serde::{Deserialize, Serialize};

const THIS_COLLECTION_NAME: &'static str = "USER_BOOK";

#[derive(Serialize, Deserialize)]
pub struct UserBook {
    pub title: String,
    pub desc: String,
    pub page_cnt: i32,
}

// get_db
fn get_db() -> Collection {
    get_user_collection(THIS_COLLECTION_NAME)
}

pub async fn insert(user_book: UserBook) -> bool {
    let result = get_db().insert_one(user_book, None).await;

    match result {
        Ok(r) => {
            println!("success. result is {:?}", r);
            true
        }
        Err(e) => {
            println!("book insert error is {:?}", e);
            false
        }
    }
}

pub async fn find_one(title: &str) -> Option<UserBook> {
    let result = get_db().find_one(doc!["title": title], None).await;

    match result {
        Ok(r) => r,
        Err(_) => None,
    }
}

// test
#[cfg(test)]
pub mod test;
