use crate::error::{Kind, Result};
use crate::gateway;
use hyper::header::CONTENT_TYPE;
use hyper::{Body, Method, Request, Response, StatusCode};

// router .
pub async fn router(req: Request<Body>) -> Result<Response<Body>> {
    println!("[INFO] {}", req.uri().path());

    let result = match (req.method(), req.uri().path()) {
        (&Method::GET, "/") => gateway::http::top::get(req),
        (&Method::GET, "/html") => gateway::http::top::get_html(req),
        (&Method::POST, "/echo") => gateway::http::echo::echo(req),
        (&Method::POST, "/echo/uppercase") => gateway::http::echo::echo_uppercase(req),
        (&Method::GET, "/echo/parameter") => gateway::http::echo::echo_form(req),
        (&Method::POST, "/echo/parameter") => gateway::http::echo::echo_parameter(req).await,
        (&Method::POST, "/book/insert") => gateway::http::book::add_book(req).await,
        (&Method::GET, "/book/get") => gateway::http::book::get_book(req).await,
        (&Method::GET, "/test/err") => gateway::http::test::get_error(req),
        (&Method::GET, "/todo/get") => gateway::http::todo_controller::get(req).await,
        (&Method::GET, "/todo/create") => gateway::http::todo_controller::create(req).await,

        // Return the 404 Not Found for other routes.
        _ => {
            let mut not_found = Response::default();
            *not_found.status_mut() = StatusCode::NOT_FOUND;
            Ok(not_found)
        }
    };

    match result {
        Ok(mut res) => {
            // setting header
            res.headers_mut().insert(
                CONTENT_TYPE,
                "application/json; charset=utf-8"
                    .parse()
                    .expect("content-typeのparseに失敗しました"),
            );

            Ok(res)
        }
        Err(e) => {
            // error
            // TODO もしかしたら、他のThreadに渡したら早くなるかも？
            // 実際に速度検証する必要あり
            println!("error is {:?}", e);
            println!("error message is {:?}", e.emsg);
            println!("trace is {:?}", e.backtrace);

            match e.inner.kind {
                Kind::Logic(_) => {
                    let mut bad_request = Response::default();
                    *bad_request.status_mut() = StatusCode::BAD_REQUEST;
                    Ok(bad_request)
                }
                _ => {
                    let mut internal_server_err = Response::default();
                    *internal_server_err.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                    Ok(internal_server_err)
                }
            }
        }
    }
}
