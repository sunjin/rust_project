use crate::infrastructure::http::router::router;
use hyper::service::make_service_fn;
use hyper::service::service_fn;
use hyper::Server;

use crate::error::Error;
use crate::error::Result;

pub mod router;

pub async fn run() -> Result<()> {
    let addr: std::net::SocketAddr = ([127, 0, 0, 1], 3000).into();

    let service = make_service_fn(|_| async { Ok::<_, Error>(service_fn(router)) });

    let server = Server::bind(&addr).serve(service);

    println!("Listening on http://{}", addr);

    server.await?;

    Ok(())
}
