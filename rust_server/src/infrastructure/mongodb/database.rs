use mongodb::Database as MongoDatabase;

use mongodb::bson::doc;
use mongodb::bson::Document;
// use bson::Document;

use crate::error::Result;

use std::iter::Iterator;

pub struct Database {
    pub db: MongoDatabase,
}

pub struct IndexModel {
    pub keys: Document, // ex ) doc!["title": 1]
    pub name: String,
}

impl IndexModel {
    pub fn new(keys: Document, name: impl Into<String>) -> IndexModel {
        IndexModel {
            keys: keys,
            name: name.into(),
        }
    }
}

impl Database {
    // create_index indexを作成する
    pub async fn create_index(
        &self,
        col_name: &str,
        index_model_list: Vec<IndexModel>,
    ) -> Result<Document> {
        let indexes: Vec<Document> = index_model_list
            .into_iter()
            .map(|index_model| doc!["key": index_model.keys, "name": index_model.name])
            .collect();
        let document = doc!["createIndexes": col_name, "indexes": indexes];

        let result = self.db.run_command(document, None).await?;

        Ok(result)
    }
}
