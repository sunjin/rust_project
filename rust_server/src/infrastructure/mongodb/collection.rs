use futures_util::stream::StreamExt;
use mongodb::options::{
    DeleteOptions, FindOneAndUpdateOptions, FindOneOptions, FindOptions, InsertManyOptions,
    InsertOneOptions, UpdateModifications, UpdateOptions,
};
use mongodb::Collection as MongoCollection;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::error::Result;

pub struct Collection {
    pub col: MongoCollection,
}

impl Collection {
    // insert_one データを一つ追加する
    pub async fn insert_one<T>(&self, data: T, opts: Option<InsertOneOptions>) -> Result<bson::Bson>
    where
        T: Serialize,
    {
        let serialized_data = bson::to_bson(&data)?;

        if let bson::Bson::Document(document) = serialized_data {
            let result = self.col.insert_one(document, opts).await?;
            return Ok(result.inserted_id);
        }
        panic!("insert_one error");
    }

    // insert_many
    pub async fn insert_many<T>(
        &self,
        data_list: Vec<T>,
        opts: Option<InsertManyOptions>,
    ) -> Result<HashMap<usize, bson::Bson>>
    where
        T: Serialize,
    {
        let mut document_list = Vec::new();
        for data in data_list {
            let serialized_data = bson::to_bson(&data)?;
            if let bson::Bson::Document(document) = serialized_data {
                document_list.push(document);
            }
        }

        let result = self.col.insert_many(document_list, opts).await?;

        Ok(result.inserted_ids)
    }

    // update_one データを１つ更新する
    pub async fn update_one(
        &self,
        query: bson::Document,
        update: UpdateModifications,
        opts: Option<UpdateOptions>,
    ) -> Result<i64> {
        let result = self.col.update_one(query, update, opts).await?;
        Ok(result.modified_count)
    }

    // update_many データを複数更新する
    pub async fn update_many(
        &self,
        query: bson::Document,
        update: UpdateModifications,
        opts: Option<UpdateOptions>,
    ) -> Result<i64> {
        let result = self.col.update_many(query, update, opts).await?;
        Ok(result.modified_count)
    }

    // delete_one データを１つ削除する
    pub async fn delete_one(
        &self,
        query: bson::Document,
        opts: Option<DeleteOptions>,
    ) -> Result<i64> {
        let result = self.col.delete_one(query, opts).await?;
        Ok(result.deleted_count)
    }

    // delete_many データを複数削除する
    pub async fn delete_many(
        &self,
        query: bson::Document,
        opts: Option<DeleteOptions>,
    ) -> Result<i64> {
        let result = self.col.delete_many(query, opts).await?;
        Ok(result.deleted_count)
    }

    // find_one データを１つ追加する
    pub async fn find_one<T>(
        &self,
        query: bson::Document,
        opts: Option<FindOneOptions>,
    ) -> Result<Option<T>>
    where
        T: for<'a> Deserialize<'a>,
    {
        let result = self.col.find_one(Some(query), opts).await?;
        match result {
            Some(document) => Ok(Some(bson::from_bson::<T>(bson::Bson::Document(document))?)),
            None => Ok(None),
        }
    }

    // find データを取得する
    pub async fn find<T>(&self, query: bson::Document, opts: Option<FindOptions>) -> Result<Vec<T>>
    where
        T: for<'a> Deserialize<'a>,
    {
        let mut vec = Vec::new();

        let mut cursor = self.col.find(Some(query), opts).await?;
        while let Some(result) = cursor.next().await {
            if let Ok(document) = result {
                vec.push(bson::from_bson::<T>(bson::Bson::Document(document))?);
            }
        }

        Ok(vec)
    }

    // find_one_and_update データを更新して取得する
    pub async fn find_one_and_update<T>(
        &self,
        query: bson::Document,
        update: UpdateModifications,
        opts: Option<FindOneAndUpdateOptions>,
    ) -> Result<Option<T>>
    where
        T: for<'a> Deserialize<'a>,
    {
        let result = self.col.find_one_and_update(query, update, opts).await?;
        match result {
            Some(document) => Ok(Some(bson::from_bson::<T>(bson::Bson::Document(document))?)),
            None => Ok(None),
        }
    }
}
