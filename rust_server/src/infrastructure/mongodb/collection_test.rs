// cargo test -- --nocapture infrastructure::mongodb::collection_test

use crate::common::test;
use crate::infrastructure::mongodb::collection::Collection;
use crate::infrastructure::mongodb::connect::*;
use mongodb::bson::doc;
use mongodb::options::UpdateModifications;
use serde::{Deserialize, Serialize};

#[tokio::test]
async fn insert_one_test() {
    // let _ = setup().await;
    let a = test::setup().await;
    if let Err(a) = a {
        panic!("setup error {:?}", a);
    }

    let data = MstTest {
        name: "sunjin".to_string(),
        age: 111,
    };

    let bson = get_db().insert_one(data, None).await;

    println!("insert result is {:?}", bson);

    if let Ok(b) = bson {
        println!("bbb is {:?}", b);
    }
}

#[tokio::test]
async fn find_one_test() {
    // setup
    let setup = test::setup().await;
    if let Err(setup) = setup {
        panic!("setup error {:?}", setup);
    }

    let query = doc!["name": "sunjin"];

    // let _ = get_db().find_one(query, None).await;

    let result = get_db().find_one::<MstTest>(query, None).await;

    println!("find one result is {:?}", result);
}

#[tokio::test]
async fn find_test() {
    // setup
    let setup = test::setup().await;
    if let Err(_setup) = setup {
        panic!("setup error ");
    }

    let query = doc!["name": "sunjin"];

    let result = get_db().find::<MstTest>(query, None).await;

    println!("find result is {:?}", result);
}

#[tokio::test]
async fn insert_many() {
    // setup
    let setup = test::setup().await;
    if let Err(_) = setup {
        panic!("setup error")
    }

    let mst_test_list: Vec<MstTest> = vec![
        MstTest {
            name: "hoge".to_string(),
            age: 123,
        },
        MstTest {
            name: "hoge2".to_string(),
            age: 321,
        },
    ];

    let result = get_db().insert_many(mst_test_list, None).await;

    println!("insert many result is {:?}", result);
}

#[tokio::test]
async fn update_one_test() {
    // setup
    let setup = test::setup().await;
    if let Err(setup) = setup {
        panic!("setup error {:?}", setup);
    }

    let query = doc!["name": "sunjin"];

    let update = doc!["$set": doc!["age": 256]];

    let result = get_db()
        .update_one(query, UpdateModifications::Document(update), None)
        .await;

    println!("update result is {:?}", result);
}

#[tokio::test]
async fn delete_one_test() {
    // se4tup
    let setup = test::setup().await;
    if let Err(setup) = setup {
        panic!("setup error {:?}", setup);
    }

    let query = doc!["name": "sunjin"];

    let result = get_db().delete_one(query, None).await;

    println!("delete result is {:?}", result);
}

#[derive(Serialize, Deserialize, Debug)]
struct MstTest {
    pub name: String,
    pub age: i32,
}

fn get_db() -> Collection {
    get_master_collection("MST_TEST")
}
