// cargo test -- --nocapture infrastructure::mongodb::mongodb_test

use crate::infrastructure::mongodb::connect::connect;
use crate::infrastructure::mongodb::connect::get_master_collection;
use crate::infrastructure::mongodb::*;
use mongodb::bson::doc;
use crate::common::config::CONFIG_DATA;
use crate::error::Result;

#[test]
fn aaa() {
    println!("aaaaaaaaaaaaa");
}

#[tokio::test]
async fn test() {
    println!("===== tokio test =====");

    // connection
    let _ = connect_mongo_test().await;

    // insert test
    let _ = insert_test().await;

    // setup test
    let err = setup_test().await;

    if let Err(e) = err {
        println!("setup err {:?}", e);
    }

    // insert2
    let _ = insert_test_2().await;

    // セットアップ後に、col取得してinsertしてみる
}

async fn setup_test() -> Result<()> {
    let _ = connect::setup().await?;
    Ok(())
}

async fn connect_mongo_test() -> Result<()> {
    let config = &*CONFIG_DATA;

    let mongo = &config.mongo_databases[0];

    let client = connect(&mongo).await?;

    // DB名取得
    for db_name in client.list_database_names(None, None).await? {
        println!("============= mongo db name is {}", db_name);

        // collection名取得

        let db = client.database(&db_name);

        for collection_name in db.list_collection_names(None).await? {
            println!("==== mongo coll name is {}", collection_name);
        }
    }

    Ok(())
}

async fn insert_test() -> Result<()> {
    let config = &*CONFIG_DATA;

    let mongo = &config.mongo_databases[0];
    let client = connect(&mongo).await?;

    let db = client.database("RUST_DB");

    let col = db.collection("MST_BOOKS");

    let docs = vec![
        doc!["title": "1984", "author": "GEorge Orwell"],
        doc!["title": "hahaha", "author": "ほん"],
    ];

    // insert
    let result = col.insert_many(docs, None).await?;

    println!("result is {:?}", result);

    Ok(())
}

async fn insert_test_2() -> Result<()> {
    let doc = doc!["name": "wine", "size": 200];

    let result = get_master_collection("MST_WINE")
        .col
        .insert_one(doc, None)
        .await?;

    println!("insert one result is {:?}", result);

    Ok(())
}
