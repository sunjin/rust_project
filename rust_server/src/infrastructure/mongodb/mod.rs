use crate::error::Result;

pub mod connect;

pub mod collection;

pub mod index;

pub mod database;

#[cfg(test)]
pub mod mongodb_test;

#[cfg(test)]
pub mod collection_test;

pub async fn init() -> Result<()> {
    // setup client
    connect::setup().await?;

    // create index
    index::create_indexis().await?;
    Ok(())
}
