use crate::error::Result;
use crate::infrastructure::mongodb::connect;
use crate::infrastructure::mongodb::database::IndexModel;
use mongodb::bson::doc;

#[cfg(test)]
pub mod test;

// create_indexis MongoDBのindex作成処理
pub async fn create_indexis() -> Result<()> {
    let user_db = connect::get_user_db();

    // USER_TODO
    user_db
        .create_index(
            "USER_TODO",
            vec![IndexModel::new(doc!["title": -1], "title_-1")],
        )
        .await?;

    Ok(())
}
