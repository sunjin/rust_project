use crate::common::config::CONFIG_DATA;
use crate::common::config::MongoDb;
use crate::error::{Error, Kind, Result};
use crate::infrastructure::mongodb::collection::Collection;
use crate::infrastructure::mongodb::database::Database;
use mongodb::{options::ClientOptions, Client};
use std::collections::HashMap;
use std::sync::Mutex;

const MASTER_DB_NAME: &str = "RUST_MASTER";
const USER_DB_NAME: &str = "RUST_USER";
const LOG_DB_NAME: &str = "RUST_LOG";

lazy_static! {
    static ref CLIENT_MAP: Mutex<HashMap<String, Client>> = Mutex::new(HashMap::new());
}

// DB初期化
pub async fn setup() -> Result<()> {
    println!("=== mongo setup");
    let mut client_map = CLIENT_MAP.lock().unwrap();

    let config = &*CONFIG_DATA;
    let mongodb_list = config.mongo_databases.iter();

    for mongodb in mongodb_list {
        client_map.insert(mongodb.dbname.clone(), connect(&mongodb).await?);
    }

    Ok(())
}

pub async fn connect(mongodb: &MongoDb) -> Result<Client> {
    // mongodb://[username:password@]host1[:port1][,...hostN[:portN]][/[defaultauthdb][?options]]
    // https://docs.mongodb.com/manual/reference/connection-string/#connection-string-formats


    let uri = format!("mongodb://{}:{}", mongodb.host, mongodb.port);
    let client_options = ClientOptions::parse(&uri).await?;

    match Client::with_options(client_options) {
        Ok(client) => Ok(client),
        Err(e) => Err(Error::new(Kind::MongoError, Some(format!("{}", e)))),
    }
}

// masterのcollectionを取得する
pub fn get_master_collection(coll_name: &str) -> Collection {
    get_db_collection(MASTER_DB_NAME, coll_name)
}

// userのcollectionを取得する
pub fn get_user_collection(coll_name: &str) -> Collection {
    get_db_collection(USER_DB_NAME, coll_name)
}

// logのcollectionを取得する
pub fn get_log_collection(coll_name: &str) -> Collection {
    get_db_collection(LOG_DB_NAME, coll_name)
}

// masterのdbを取得する
pub fn get_master_db() -> Database {
    get_db(MASTER_DB_NAME)
}

// userのdbを取得する
pub fn get_user_db() -> Database {
    get_db(USER_DB_NAME)
}

// logのdbを取得する
pub fn get_log_db() -> Database {
    get_db(LOG_DB_NAME)
}

// dbを取得する
fn get_db(db_name: &'static str) -> Database {
    let client_map = CLIENT_MAP.lock().unwrap();
    if let Some(client) = client_map.get(db_name) {
        return Database {
            db: client.database(&db_name),
        };
    }

    panic!("client get error");
}

// collectionを取得する
fn get_db_collection(db_name: &'static str, coll_name: &str) -> Collection {
    let db = get_db(db_name);
    Collection {
        col: db.db.collection(&coll_name),
    }
}
