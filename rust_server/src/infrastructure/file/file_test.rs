// cargo test -- --nocapture infrastructure::file::file_test
// cargo test -- --test-threads=1 infrastructure::file::file_test

use crate::common::config;
use crate::infrastructure::file;
use crate::common::config::CONFIG_DATA;


#[test]
pub fn deserial_toml_file_test() {

    let config = &*CONFIG_DATA;

    println!("config is {:?}", config);



    let obj = file::toml::deserial_toml_file::<config::Config>("./config/local.toml");

    println!("obj is {:?}", obj);
}
