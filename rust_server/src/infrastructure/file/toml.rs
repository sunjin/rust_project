use crate::error::Result;
use serde::de::DeserializeOwned;
use serde::Deserialize;
use std::fs;
use toml;

// repositoryに各予定
#[derive(Debug, Deserialize)]
pub struct Config {
    pub mongo_db: MongoDb,
}

#[derive(Debug, Deserialize)]
pub struct MongoDb {
    pub name: String,
    pub age: i32,
}

// deserial_toml_file tomlのfileをstructにdeserializeする
pub fn deserial_toml_file<T>(path: &str) -> Result<T>
where
    T: DeserializeOwned,
{
    let file_str = fs::read_to_string(path)?;

    let obj = toml::from_str::<T>(&file_str)?;

    Ok(obj)
}
