use std::convert::From;
use std::error::Error as StdError;
use std::fmt;

use backtrace::Backtrace;
use bson::de::Error as BsonDeserializeError;
use bson::ser::Error as BsonSerializeError;
use hyper::Error as HyperError;
use mongodb::error::Error as MongoError;
use serde_json::error::Error as SerdeJsonError;
use std::io::Error as IoError;
use toml::de::Error as TomlError;

pub type Result<T> = std::result::Result<T, Error>;

type Cause = Box<dyn StdError + Send + Sync>;

// このプロジェクト処理中に発生する可能性のあるエラーを表します
pub struct Error {
    pub inner: Box<ErrorImpl>,
    pub backtrace: Backtrace,
    pub emsg: Option<String>,
}

impl StdError for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        self.inner
            .cause
            .as_ref()
            .map(|cause| &**cause as &(dyn StdError + 'static))
    }
}

// hyper error
impl From<HyperError> for Error {
    fn from(e: HyperError) -> Self {
        Error::new(Kind::HyperError, Some(format!("http error : {}", e)))
    }
}

// mongo error
impl From<MongoError> for Error {
    fn from(e: MongoError) -> Self {
        Error::new(Kind::MongoError, Some(format!("mongo error : {}", e)))
    }
}

// bson serialize error
impl From<BsonSerializeError> for Error {
    fn from(e: BsonSerializeError) -> Self {
        Error::new(
            Kind::BsonSerializeError,
            Some(format!("bson serialize error : {}", e)),
        )
    }
}

// bson desirialize error
impl From<BsonDeserializeError> for Error {
    fn from(e: BsonDeserializeError) -> Self {
        Error::new(
            Kind::BsonDeserializeError,
            Some(format!("bson deserialize error : {}", e)),
        )
    }
}

// serde_json error
impl From<SerdeJsonError> for Error {
    fn from(e: SerdeJsonError) -> Self {
        Error::new(
            Kind::SerdeJsonError,
            Some(format!("serde json error : {}", e)),
        )
    }
}

// file io error
impl From<IoError> for Error {
    fn from(e: IoError) -> Self {
        Error::new(Kind::IoError, Some(format!("io error : {}", e)))
    }
}

// toml error
impl From<TomlError> for Error {
    fn from(e: TomlError) -> Self {
        Error::new(Kind::TomlError, Some(format!("toml error : {}", e)))
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.emsg {
            Some(emsg) => write!(f, "{:?}: {}\n{:?}", self.inner.kind, emsg, self.backtrace),
            None => write!(f, "{:?}\n{:?}", self.inner.kind, self.backtrace),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.emsg {
            Some(emsg) => write!(f, "{:?}: {}", self.inner.kind, emsg),
            None => write!(f, "{:?}", self.inner.kind),
        }
    }
}
pub struct ErrorImpl {
    pub kind: Kind,
    cause: Option<Cause>,
}

#[derive(Debug, PartialEq)]
pub enum Kind {
    Logic(Logic),
    System(System),
    HyperError,
    MongoError,
    BsonSerializeError,
    BsonDeserializeError,
    NoneError,
    SerdeJsonError,
    IoError,
    TomlError,
}

// ロジックエラー
#[derive(Debug, PartialEq)]
pub enum Logic {
    // 本が見つかりませんでした
    NotFoundBook,

    // やる気が出ませんでした
    DidNotFeelMotivated,

    // 必須パラメータが見つかりません
    ParamNotFound,

    // Queryが見つかりませんでした
    NotFoundQuery,
}

// システムエラー
#[derive(Debug, PartialEq)]
pub enum System {
    // DBが見つかりませんでした
    DBNotFound,

    // Redisが見つかりませんでした
    RedisNotFound,
}

// impl
impl Error {
    pub fn new_logic(logic: Logic, emsg: &str) -> Error {
        Error::new(Kind::Logic(logic), Some(emsg.to_string()))
    }

    pub fn new_system(system: System, emsg: &str) -> Error {
        Error::new(Kind::System(system), Some(emsg.to_string()))
    }

    // logic errorかどうかを判定
    pub fn is_logic(&self) -> bool {
        match self.inner.kind {
            Kind::Logic(_) => true,
            _ => false,
        }
    }

    // system errorかどうかを判定
    pub fn is_system(&self) -> bool {
        match self.inner.kind {
            Kind::System(_) => true,
            _ => false,
        }
    }

    // errorの作成
    pub fn new(kind: Kind, emsg: Option<String>) -> Error {
        Error {
            inner: Box::new(ErrorImpl { kind, cause: None }),
            backtrace: Backtrace::new(),
            emsg: emsg,
        }
    }
}
