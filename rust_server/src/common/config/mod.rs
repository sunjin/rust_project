use crate::infrastructure::file::toml;
use clap::{App, Arg};
use serde::Deserialize;
// use std::sync::{Arc, Mutex, Once, ONCE_INIT};

#[derive(Debug, Deserialize, Default)]
pub struct Config {
    pub port: String, // ap port
    pub mongo_databases: Vec<MongoDb>,
}

#[derive(Debug, Deserialize, Default)]
pub struct MongoDb {
    pub host: String,
    pub port: String,
    pub dbname: String,
    pub user: String,
    pub password: String,
}

// #[derive(Clone)]
// pub struct SingletonReader {
//     inner: Arc<Mutex<Config>>,
// }

// pub fn init(path: String) {

//     static mut SINGLETON: Option<Box<SingletonReader>> = None;
//     static ONCE: Once = ONCE_INIT;

//     unsafe {
//         ONCE.call_once(|| {
//             let singleton = SingletonReader {
//                 inner
//             }
//         });
//     }

// }

// 初回呼び出し時に初期化
lazy_static! {
    pub static ref CONFIG_DATA: Config = {
        println!("=== config setup");

        let arg = App::new("rust_server").arg(
            Arg::with_name("config_file")
                .help("config file pathを指定")
                .short("f")
                .long("configfile")
                .default_value("./config/local.toml"),
        );

        let matches = arg.get_matches();
        let path = matches
            .value_of("config_file")
            .expect("config fileのpathが存在しません");

        toml::deserial_toml_file::<Config>(path).expect("config fileの変換に失敗しました")
    };
}
