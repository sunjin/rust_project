use crate::error::Result;
use crate::infrastructure::mongodb::connect;

// setup mongodbやredisのsetupをする
pub async fn setup() -> Result<()> {
    connect::setup().await?;
    Ok(())
}
