use crate::repository::mongodb::user::user_book_repository::{find_one, UserBook};

// bookを取得する
pub async fn get_book(title: &str) -> Option<UserBook> {
    find_one(title).await
}
