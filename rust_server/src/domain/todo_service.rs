use crate::error::Result;
use crate::repository::mongodb::user::user_todo_repository;

// todoを追加する
pub async fn add_todo(title: &str, desc: &str) -> Result<()> {
    // TODO 文字数制限チェック

    // create
    user_todo_repository::insert(title, desc).await
}

// todo liset get
pub async fn get() -> Result<Vec<user_todo_repository::UserTodo>> {
    user_todo_repository::find_all().await
}
