#![deny(warnings)]
#![warn(rust_2018_idioms)]

use futures_util::future::join;
use hyper::{Body, Request, Response, Server};
use hyper::service::{make_service_fn, service_fn};

static INDEX1: &[u8] = b"The 1st service!";
static INDEX2: &[u8] = b"The 2nd service!";

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>>{
    println!("Hello, world!");

    pretty_env_logger::init();

    let addr1 = ([127, 0, 0, 1], 1337).into();
    let addr2 = ([127, 0, 0, 1], 1338).into();

    let srv1 = Server::bind(&addr1).serve(make_service_fn(|_| async {
        Ok::<_, hyper::Error>(service_fn(index1))
    }));

    let srv2 = Server::bind(&addr2).serve(make_service_fn(|_| async {
        Ok::<_, hyper::Error>(service_fn(index2))
    }));

    println!("Listening on http://{} and http://{}", addr1, addr2);

    let _ = join(srv1, srv2).await;

    Ok(())
}


async fn index1(_: Request<Body>) -> Result<Response<Body>, hyper::Error> {
    Ok(Response::new(Body::from(INDEX1)))
}

async fn index2(_: Request<Body>) -> Result<Response<Body>, hyper::Error> {
    Ok(Response::new(Body::from(INDEX2)))
}